from rest_framework import serializers
from btree.models import Tree


class TreeSerializer(serializers.ModelSerializer):
    class Meta:
        model = Tree
        fields = ['id', 'name']


class PreOrder(object):
    def __init__(self, id, level, value, parentValue):
        self.id = id
        self.level = level
        self.value = value
        self.parent = parentValue


class PreOrderSerializer(serializers.Serializer):
    id = serializers.IntegerField()
    level = serializers.IntegerField()
    value = serializers.IntegerField()
    parent = serializers.IntegerField()
