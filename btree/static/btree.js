Vue.config.devtools = true
const snapInstances = {}

new Vue({
  el: '#app',
  delimiters: ['${', '}'],
  data: {
    trees: [],
    treeName: '',
    nodeValue: undefined,
    nodeValue1: undefined,
    nodeValue2: undefined,
    validationName: undefined,
    validationValue: undefined,
    selectedTree: null
  },
  async mounted(){
    this.trees = await api.getTrees()
  },
  methods: {
    toggle(tree){
      if(this.selectedTree){
        this.$root.$emit('bv::toggle::collapse', `tree-${this.selectedTree.id}`)
        if(this.selectedTree !== tree){
          this.selectedTree = tree
          this.$root.$emit('bv::toggle::collapse', `tree-${tree.id}`)
          setTimeout(() => this.drawTree(tree), 100)
        }
        else{
          this.selectedTree = null
        }
      }
      else{
        this.selectedTree = tree
        this.$root.$emit('bv::toggle::collapse', `tree-${tree.id}`)
        setTimeout(() => this.drawTree(tree), 100)
      }
    },
    async addTree($event){
      $event.preventDefault()
      if(!this.validationName) return this.validationName = false
      await api.postTree(this.treeName)
      this.trees = await api.getTrees()
    },
    async removeTree(tree){
      await api.deleteTree(tree.id)
      this.trees = await api.getTrees()
    },
    async addNode($event){
      $event.preventDefault()
      if(!this.validationValue) return
      try{
        await api.postNode({ treeId: this.selectedTree.id, value: Number(this.nodeValue)})
      }catch(e){
        alert('Repeated node!')
      }
      this.nodeValue = ''
      this.validationValue = undefined
      this.drawTree(this.selectedTree)
    },
    async calcLowestAncestor($event){
      $event.preventDefault()
      try{
        this.selectedTree.commonAncestor = await api.lowestAncestor({
          treeId: this.selectedTree.id,
          value1: this.nodeValue1,
          value2: this.nodeValue2
        })
        if(!this.selectedTree.commonAncestor){
          alert('Common ancestor not found')
        }
      }catch(e){
        console.error(e)
        alert('You typed an unknown node. Please check!')
      }
      this.drawTree(this.selectedTree)
    },
    async drawTree(tree){
      snapInstances[tree.id] = snapInstances[tree.id] || Snap(this.$refs[`draw-${tree.id}`][0])
      let snap = snapInstances[tree.id]
      snap.clear()

      let preorderTree = await api.preorder(tree.id)
      this.drawLines(preorderTree, snap)
      for(let i = 0; i < preorderTree.length; i++){
        let node = preorderTree[i]
        let {x, y} = this.calcPosition(preorderTree, node)

        let circle = snap.circle(x, y, 15)
        let text = snap.text(x - 6, y + 4, node.value)
        if(node.value === tree.commonAncestor){
          circle.attr({fill: '#ff0000', stroke: '#005500', strokeWidth: 2})
          text.attr({fontSize: 10, color: '#fff'})
        }
        else{
          circle.attr({fill: '#fff', stroke: '#005500', strokeWidth: 2})
          text.attr({fontSize: 10})
        }
      }
    },
    drawLines(preorderTree, snap){
      for(let i = 0; i < preorderTree.length; i++){
        let node = preorderTree[i]
        let positionChild = this.calcPosition(preorderTree, node)
        if(node.parent){
          let parent = preorderTree.find(otherNode => otherNode.value === node.parent)
          let positionParent = this.calcPosition(preorderTree, parent)
          snap.line(positionChild.x, positionChild.y, positionParent.x, positionParent.y).attr({
            stroke: '#000',
            strokeWidth: 2
          })
        }
      }
    },
    calcPosition(preorderTree, node){
      let factor = 40
      let x = (1 + preorderTree.indexOf(node)) * factor
      let y = (1 + node.level) * factor
      return {x, y}
    }
  }
});