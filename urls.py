from django.contrib import admin
from django.urls import path, include
from btree import views


urlpatterns = [
    path('admin/', admin.site.urls),
    path('', views.index),
    path('api/trees', views.get_trees),
    path('api/tree', views.post_tree),
    path('api/tree/<int:tree_id>', views.tree_instance_view),
    path('api/tree/<int:tree_id>/lowest-common-ancestor', views.get_lowest_common_ancestor),
    path('api-auth/', include('rest_framework.urls', namespace='rest_framework'))
]
