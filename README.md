# Binary Tree Test

## Requirements

- Install `docker` (recommended version = 19.03.1-ce)
- Install `docker-compose` (recommended version = 1.24.1)

## Usage

- `docker-compose build`
- `docker-compose up postgres django`

## Tests

- `docker-compose build`
- `docker-compose up test`