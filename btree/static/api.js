window.api = {
  async getTrees(){
    let response = await axios.get('/api/trees')
    return response.data
  },

  async postTree(name){
    let response = await axios.post('/api/tree', { name })
    return response.data
  },

  async postNode({ treeId, value }){
    let response = await axios.post(`/api/tree/${treeId}`, { value })
    return response.data
  },

  async preorder(treeId){
    let response = await axios.get(`/api/tree/${treeId}`)
    return response.data
  },

  async deleteTree(treeId){
    let response = await axios.delete(`/api/tree/${treeId}`)
    return response.data
  },

  async lowestAncestor({treeId, value1, value2}){
    let response = await axios.post(`/api/tree/${treeId}/lowest-common-ancestor`, {
      value1, value2
    })
    return response.data
  }
}