from django.db import models


class Tree(models.Model):
    name = models.CharField(max_length=200)
    root = models.ForeignKey('btree.Node', on_delete=models.SET_NULL, null=True, related_name='root_node')


class Node(models.Model):
    value = models.IntegerField(default=0)
    tree = models.ForeignKey('btree.Tree', on_delete=models.CASCADE)
    left = models.ForeignKey('self', on_delete=models.SET_NULL, null=True, related_name='left_node')
    right = models.ForeignKey('self', on_delete=models.SET_NULL, null=True, related_name='right_node')
    parent = models.ForeignKey('self', on_delete=models.CASCADE, null=True, related_name='parent_node')
