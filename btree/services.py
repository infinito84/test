from btree.models import Tree, Node
from btree.serializers import PreOrder
from django.shortcuts import get_object_or_404

class BTreeService():

    @staticmethod
    def get_trees():
        return Tree.objects.all()

    @staticmethod
    def create_tree(name):
        tree = Tree(name=name)
        tree.save()
        return tree

    @staticmethod
    def add_node(tree, value):
        node = NodeService.create_node(tree, value)

        if not tree.root:
            tree.root = node
            tree.save()
            return

        NodeService.add_node(tree.root, node)

    @staticmethod
    def lowest_common_ancestor(tree, value1, value2):
        node1 = get_object_or_404(Node, value=value1, tree=tree)
        node2 = get_object_or_404(Node, value=value2, tree=tree)
        parents_dict = {}
        while node1.parent:
            parents_dict[node1.parent.value] = node1.parent
            node1 = node1.parent
        while node2.parent:
            if node2.parent.value in parents_dict:
                return node2.parent.value
            node2 = node2.parent


class NodeService:

    @staticmethod
    def create_node(tree, value):
        node = Node(tree=tree, value=value)
        node.save()
        return node

    @staticmethod
    def add_node(parent, node):
        if parent.value == node.value:
            node.delete()
            raise Exception('This value has already added')

        if node.value < parent.value:
            if not parent.left:
                parent.left = node
                node.parent = parent
                node.save()
                parent.save()
            else:
                NodeService.add_node(parent.left, node)

        if node.value > parent.value:
            if not parent.right:
                parent.right = node
                node.parent = parent
                node.save()
                parent.save()
            else:
                NodeService.add_node(parent.right, node)

    @staticmethod
    def preorder(node, preorder_list, level):
        if node:
            NodeService.preorder(node.left, preorder_list, level + 1)
            preorder_list.append(PreOrder(
                node.id,
                level,
                node.value,
                node.parent.value if node.parent else None
            ))
            NodeService.preorder(node.right, preorder_list, level + 1)
        return preorder_list
