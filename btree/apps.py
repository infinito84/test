from django.apps import AppConfig


class BTreeConfig(AppConfig):
    name = 'btree'
