import traceback
from django.shortcuts import render, get_object_or_404
from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework import status

from btree.services import BTreeService, NodeService
from btree.serializers import TreeSerializer, PreOrderSerializer
from btree.models import Tree, Node


def index(request):
    return render(request, 'index.html', {})


@api_view(['GET'])
def get_trees(request):
    trees = BTreeService.get_trees()
    return Response(TreeSerializer(trees, many=True).data)


@api_view(['POST'])
def post_tree(request):
    serializer = TreeSerializer(data=request.data)
    if serializer.is_valid():
        serializer.save()
        return Response(serializer.data, status=status.HTTP_201_CREATED)
    return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


@api_view(['GET', 'POST', 'DELETE'])
def tree_instance_view(request, tree_id):
    tree = get_object_or_404(Tree, id=tree_id)

    if request.method == 'GET':
        preorder_list = NodeService.preorder(tree.root, [], 0)
        return Response(PreOrderSerializer(preorder_list, many=True).data)

    if request.method == 'POST':
        try:
            BTreeService.add_node(tree, request.data['value'])
            return Response({}, status=status.HTTP_201_CREATED)
        except Exception:
            traceback.print_exc()
            return Response({}, status=status.HTTP_409_CONFLICT)

    if request.method == 'DELETE':
        tree.delete()
        return Response({})


@api_view(['POST'])
def get_lowest_common_ancestor(request, tree_id):
    tree = get_object_or_404(Tree, id=tree_id)

    if not request.data or not request.data.get('value1') or not request.data.get('value2'):
        return Response({}, status=status.HTTP_409_CONFLICT)

    value1 = request.data['value1']
    value2 = request.data['value2']

    common_parent = BTreeService.lowest_common_ancestor(tree, value1, value2)
    return Response(common_parent)
