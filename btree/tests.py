from django.test import TestCase
from btree.services import BTreeService


class BTreeTest(TestCase):

    def test_create_and_get_trees(self):
        BTreeService.create_tree('test_tree')
        trees = BTreeService.get_trees()
        self.assertEqual(trees.count(), 1)

    def test_lowest_common_ancestor(self):
        tree = BTreeService.create_tree('test_tree2')
        BTreeService.add_node(tree, 67)
        BTreeService.add_node(tree, 39)
        BTreeService.add_node(tree, 28)
        BTreeService.add_node(tree, 29)
        BTreeService.add_node(tree, 44)
        BTreeService.add_node(tree, 76)
        BTreeService.add_node(tree, 74)
        BTreeService.add_node(tree, 85)
        BTreeService.add_node(tree, 83)
        BTreeService.add_node(tree, 87)

        self.assertEqual(39, BTreeService.lowest_common_ancestor(tree, 29, 44))
        self.assertEqual(67, BTreeService.lowest_common_ancestor(tree, 44, 85))
        self.assertEqual(85, BTreeService.lowest_common_ancestor(tree, 83, 87))
